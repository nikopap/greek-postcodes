from scrapy.exporters import CsvItemExporter


class TKExporter(CsvItemExporter):
    def __init__(self, *args, **kwargs):
        kwargs["include_headers_line"] = False
        kwargs["encoding"] = "utf-8"
        kwargs["delimiter"] = "\t"
        super(TKExporter, self).__init__(*args, **kwargs)
