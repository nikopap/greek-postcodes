# Greek Postcodes

A scraping tool for Greek postal code data. The data format is a tab-delimited format compatible with the [GeoNames postal code data format](https://download.geonames.org/export/zip/).

The data is released at this project's public "[Package Registry](https://gitlab.com/nikopap/greek-postcodes/-/packages)".

## Postal Code Data

If you notice the data is inaccurate or incomplete, please raise an [Issue](https://gitlab.com/nikopap/greek-postcodes/-/issues) for it to be fixed.

**NOTICE: THE DATA IS PROVIDED "AS IS" WITHOUT WARRANTY OR ANY REPRESENTATION OF ACCURACY, TIMELINESS OR COMPLETENESS.**
