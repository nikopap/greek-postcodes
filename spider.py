import scrapy
import re
from exporter import TKExporter

BASE_URL = "https://www.taxidromikoskodikas.gr"

STATE_MAP = dict(
    [(n, "ΑΤΤΙΚΗ") for n in range(10, 20)] +
    [(n, "ΠΕΛΟΠΟΝΝΗΣΟΣ") for n in range(20, 25)] +
    # ΑΧΑΙΑ, ΗΛΕΙΑ
    [(n, "ΔΥΤΙΚΗ ΕΛΛΑΔΑ") for n in range(25, 28)] +
    [(n, "ΙΟΝΙΑ ΝΗΣΙΑ") for n in range(28, 30)] +
    [(n, "ΣΤΕΡΕΑ ΕΛΛΑΔΑ") for n in range(32, 37)] +
    [(n, "ΘΕΣΣΑΛΙΑ") for n in range(37, 44)] +
    [(n, "ΗΠΕΙΡΟΣ") for n in range(44, 49)] +
    [(n, "ΔΥΤΙΚΗ ΜΑΚΕΔΟΝΙΑ") for n in range(50, 54)] +
    [(n, "ΚΕΝΤΡΙΚΗ ΜΑΚΕΔΟΝΙΑ") for n in range(54, 64)] +
    [(n, "ΑΝΑΤΟΛΙΚΗ ΜΑΚΕΔΟΝΙΑ ΚΑΙ ΘΡΑΚΗ") for n in range(64, 70)] +
    [(n, "ΚΡΗΤΗ") for n in range(70, 75)] +
    [(n, "ΒΟΡΕΙΟ ΑΙΓΑΙΟ") for n in range(81, 84)] +
    [(n, "ΝΟΤΙΟ ΑΙΓΑΙΟ") for n in range(84, 86)] +
    [
        # ΛΕΥΚΑΔΑ            ΚΕΡΚΥΡΑ
        (31, "ΙΟΝΙΑ ΝΗΣΙΑ"), (49, "ΙΟΝΙΑ ΝΗΣΙΑ"),
        # ΑΙΤΩΛΟΑΚΑΡΝΑΝΊΑ      ΚΥΘΗΡΑ
        (30, "ΔΥΤΙΚΗ ΕΛΛΑΔΑ"), (80, "ΑΤΤΙΚΗ")
    ]
)

STATE_CODE_MAP = {
    "ΑΤΤΙΚΗ": "I",
    "ΠΕΛΟΠΟΝΝΗΣΟΣ": "J",
    "ΔΥΤΙΚΗ ΕΛΛΑΔΑ": "G",
    "ΙΟΝΙΑ ΝΗΣΙΑ": "F",
    "ΣΤΕΡΕΑ ΕΛΛΑΔΑ": "H",
    "ΘΕΣΣΑΛΙΑ": "E",
    "ΗΠΕΙΡΟΣ": "D",
    "ΔΥΤΙΚΗ ΜΑΚΕΔΟΝΙΑ": "C",
    "ΚΕΝΤΡΙΚΗ ΜΑΚΕΔΟΝΙΑ": "B",
    "ΑΝΑΤΟΛΙΚΗ ΜΑΚΕΔΟΝΙΑ ΚΑΙ ΘΡΑΚΗ": "A",
    "ΚΡΗΤΗ": "M",
    "ΒΟΡΕΙΟ ΑΙΓΑΙΟ": "K",
    "ΝΟΤΙΟ ΑΙΓΑΙΟ": "L"
}


class TKSpider(scrapy.Spider):
    name = "tk"
    start_urls = [
        BASE_URL + "/tk"
    ]
    custom_settings = {
        "FEED_EXPORTERS": {
            "csv": TKExporter
        }
    }

    def parse(self, response):
        for a in response.xpath('//a[contains(@href, "/tk")]'):
            href = a.attrib["href"]
            search = re.search(r"^/tk/([0-9]{5})$", href)

            if search is None:
                continue

            yield response.follow(
                href,
                callback=self.parse_tk,
                cb_kwargs={"tk": search.group(1)}
            )

    def parse_tk(self, response, **kwargs):
        postal = kwargs.get("tk")
        first_row = response.xpath('//table[@id="data1"]/tr[2]')
        city = first_row.xpath('//td[3]/text()').get().strip()
        perfecture = first_row.xpath('//td[4]/text()').get().strip()
        state = STATE_MAP[int(postal[0:2])]
        state_code = STATE_CODE_MAP[state]

        yield {
            "country_code": "GR",
            "postal_code": postal,
            "place_name": city + ", " + perfecture,
            "state": state,
            "state_code": state_code,
            "county/province": "",
            "community": "",
            "latitude": "",
            "longitude": "",
            "accuracy": "",
        }
